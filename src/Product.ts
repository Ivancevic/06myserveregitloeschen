export class Product{
    private static lastId: number = 1;
    private static filterId: number = 1;
    private static filterCategory: string;
    private static filterColor: string;
    private id: number;
    private name: string;
    private category: string;
    private color: string;

    public constructor (name:string, category:string, color:string){
        this.id = Product.lastId;
        Product.lastId++;
        this.name = name;
        this.category = category;
        this.color = color;    
    }

    public getId(): number{
        return this.id;
    }
    public getName(): string{
        return this.name;
    }
    public getCategory(): string{
        return this.category;
    }
    public getColor(): string{
        return this.color;
    }
    public toString(): string{
        return "Product: [" + this.id + ", " + this.name + ", " + this.category + ", " + this.color + "]";
    }
    public static fromJson(data:any):Product{
        let retProduct: Product = new Product(data.name, data.category, data.color);
        retProduct.id = data.id;
        return retProduct;
    }
    public static setFilterId(id:number){
        Product.filterId = id;
    }
    public static setFilterCategory(category:string){
        Product.filterCategory = category;
    }
    public static setFilterColor(color:string){
        Product.filterColor = color;
    }
    public setName(name: string): void {
        this.name = name;      
    }       
    public setCategory (category: string): void{        
        this.category = category;       
    } 
    public setColor( color: string):void{       
        this.color = color;      
    }
    public static filterByCatAndColor(filterProduct: Product):boolean{
        let retProductOK:boolean = false;
        if(Product.filterCategory != null && Product.filterColor != null && Product.filterCategory == filterProduct.category && Product.filterColor == filterProduct.color){
            retProductOK = true;
        }else{
            if(Product.filterCategory != null && Product.filterColor != null && Product.filterCategory == filterProduct.category){
                retProductOK = true;
            }else{
                if(Product.filterColor != null && Product.filterCategory == null && Product.filterColor == filterProduct.color){
                    retProductOK = true;
                }
            }
        }
        return retProductOK;
    }
}