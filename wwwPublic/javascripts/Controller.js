"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
const AppError_1 = require("./AppError");
const Database_1 = require("./Database");
const FileOptions_1 = require("./FileOptions");
const Product_1 = require("./Product");
class Controller {
    constructor() {
        Controller.options = new FileOptions_1.FileOptions();
        Controller.options.root = "/Users/Selina/Desktop/5AAIF_6AAIF/NVS_ORG/sourceVSCode/06MyServer/06MyServerB/wwwPublic";
        Controller.db = new Database_1.Database();
    }
    getAllProducts(req, res) {
        let collFilteredProducts = Controller.db.getFilteredProducts(req.query.category, req.query.color);
        console.log("===== get all or filtered products");
        res.json(collFilteredProducts);
    }
    getProductById(req, res, next) {
        let product;
        try {
            console.log("===== get product by id");
            product = Controller.db.getProductById(req.params.id);
            res.json(product);
        }
        catch (error) {
            let err = new AppError_1.AppError(error.message);
            err.status = "fail";
            err.statusCode = 404;
            next(err);
        }
    }
    initProducts(req, res) {
        Controller.db.initProducts();
        console.log("===== init done");
        res.json("init done");
    }
    showErrorPage(req, res) {
        console.log("===== error page ..." + req.originalUrl);
        res.status(404).json({
            status: "fail",
            message: "Can't find " + req.originalUrl + " on this server!"
        });
    }
    doErrorHandling(err, req, res, next) {
        console.log("===== error handling ..." + req.originalUrl + "..." + JSON.stringify(err));
        err.statusCode = err.statusCode || 404;
        err.status = err.status || "unknown error";
        res.status(err.statusCode).json(err);
    }
    addProduct(req, res, next) {
        try {
            let newProduct = Product_1.Product.fromJson(req.body);
            newProduct = Controller.db.addProduct(newProduct);
            console.log("===== add product: " + newProduct.toString());
            res.json(newProduct);
        }
        catch (error) {
            let appError = new AppError_1.AppError("error: " + error.message);
            appError.status = "fail";
            appError.statusCode = 400;
            next(appError);
        }
    }
    updateProductById(req, res, next) {
        try {
            let updateProduct = Product_1.Product.fromJson(req.body);
            updateProduct = Controller.db.updateProduct(updateProduct);
            console.log("===== update product: " + updateProduct.toString());
            res.json(updateProduct);
        }
        catch (error) {
            let appError = new AppError_1.AppError("error: " + error.message);
            appError.status = "fail";
            appError.statusCode = 400;
            next(appError);
        }
    }
    deleteProductById(req, res, next) {
        try {
            const productId = req.params.id;
            Controller.db.deleteProduct(productId);
            res.status(204).end();
        }
        catch (error) {
            let appError = new AppError_1.AppError("error: " + error.message);
            appError.status = 'fail';
            appError.statusCode = 400;
            next(appError);
        }
    }
    showIndexPage(req, res) {
        res.sendFile("html/index.html", Controller.options);
        console.log("===== show index.html");
    }
    showImagesAndCSS(req, res) {
        let url = req.originalUrl;
        console.log("===== show images/CSS... " + url);
        res.sendFile(url, Controller.options);
    }
}
exports.Controller = Controller;
//# sourceMappingURL=Controller.js.map